const express = require('express');
const app = express();
var morgan = require('morgan');

const PORT = 3000;

app.use(morgan('tiny'));

let count = 0;

app.get('*', (req, res) => {
  count++;
  res.status(200).send(`<h1>Hello Docker!</h1> <br /> <div>This number is increasing: ${count}</div>`);
});

app.listen(PORT || 3000, '0.0.0.0', () => { console.log(`Listening to port ${PORT}`) });